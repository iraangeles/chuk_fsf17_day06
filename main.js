//Load libraries
var path = require("path");
var express = require("express");

//Create an instance of express
var app = express();

//Define routes
app.use("/libs", express.static(path.join(__dirname, "bower_components")));

app.use(express.static(path.join(__dirname, "public")));

//Define a port
app.set("port", process.env.APP_PORT || 3000);

//Start the app
app.listen(app.get("port"), function() {
    console.log("Application started at %s on port %d",
            new Date(), app.get("port"));
});